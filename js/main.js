//que oculte las caras cuando el documento este cargado.
$(document).ready(function(){
    oculto();

    }           
);

//cuando haga clic en la id enviar del documento html ejecuta la funcion event.
$(document).on("click", "#enviar", function (event) { 
    oculto();
    //funcion jquery validation.
    $("#formulario").validate({

        //configuracion de la libreria
        ignore: [],
        onfocusout: true,
        wrapper: "li",
        errorClass: "is-invalid",

        //reglas de validacion
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 16

            },
            lastname: {
                required: true,
                minlength: 3,
                maxlength: 16
            }
        },

        //mensajes del formulario
        messages: {
            name: {
                required: "El nombre es obligatorio",
                minlength: "El mínimo de caracteres permitidos para el nombre son 3",
                maxlength: "El máximo de caracteres permitidos para el nombre son 16",
 
            },
            lastname: {
                required: "El apellido es obligatorio",
                minlength: "El mínimo de caracteres permitidos para los apellidos son 3",
                maxlength: "El máximo de caracteres permitidos para los apellidos son 30",

            }
        }
    });

    //condicional para mostrar la cara feliz si es valido.
    if($("#formulario").valid()){
        $("#smile").show();
        event.preventDefault();

    
    }

    // condicional para mostrar la cara triste si no es valido(caso anterior espedificado en el if de arriba).
    else{

        $("#sad").show();
        event.preventDefault(); 

    }

});

//funcion para ocultar las imagenes de las caras previamente cargadas. 
function oculto (){

    $(".faces").hide();    
        
}

//con css ocultaremos las clases donde esten las imagenes y las iremos mostrando con 
// add class para agregar una clase
// remove class para borrar una clase
// display none para ocultarlas

   
        




        

        
      


       


